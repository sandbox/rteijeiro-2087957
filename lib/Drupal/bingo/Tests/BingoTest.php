<?php

/**
 * @file
 * Definition of Drupal\bingo\Tests\BingoTest.
 */

namespace Drupal\bingo\Tests;

use Drupal\simpletest\WebTestBase;

class BingoTest extends WebTestBase {

  // Enable bingo module.
  public static $modules = array('bingo');

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Bingo Test',
      'description' => 'Test to verify that Bingo module is working properly.',
      'group' => 'Bingo', 
    ); 
  } 

  /**
   * Test adding participants to database.
   *
   * Adds a random participant name and looks if status message is right.
   */
  function testAdd() {
    $name = $this->randomName(8);

    $this->drupalPostForm('bingo/add', array('name' => $name), t('Add'));
    $this->assertRaw(t('Added %name as new participant.', array('%name' => $name)));
  }

  /**
   * Test deleting participants in database.
   *
   * Creates and deletes a random participant an looks if status message is right.
   */
  function testDelete() {
    $name = $this->randomName(8);

    $this->drupalPostForm('bingo/add', array('name' => $name), t('Add'));
    $this->drupalGet('bingo');
    $this->assertRaw($name);

    $this->drupalPostForm('bingo/delete/' . $name, array(), t('Delete'));
    $this->assertRaw(t('Participant %name deleted.', array('%name' => $name)));
  }
} 

