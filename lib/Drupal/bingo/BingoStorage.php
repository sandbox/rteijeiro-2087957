<?php

namespace Drupal\bingo;

class BingoStorage {

  // Returns all participant names stored in database.
  static function getAll() {
    return db_query('SELECT name FROM {bingo}')->fetchCol();
  }

  /*
   * Stores participant name in database.
   *
   * @param $name
   *   participant name.
   */
  static function add($name) {
    db_insert('bingo')->fields(array('name' => $name))->execute();
  }

  /**
   * Deletes participant name in database.
   *
   * @param $name
   *   participant name.
   */
  static function delete($name) {
    db_delete('bingo')->condition('name', $name)->execute();
  }

  /**
   * Get participant winning count.
   *
   * @param $name
   *  participant name.
   *
   * @return $count
   *  number of times the given participant has won.
   */
  static function getWinningCount($name) {
    return db_query('
      SELECT count 
      FROM {bingo}
      WHERE name = :name', array(':name' => $name))->fetchObject();
  }

  /**
   * Increments participant winning counter.
   *
   * @param $name
   *  participant name.
   *
   * @return $updated
   *  number of rows updated.
   */
  static function incrementWinning($name) {
    // Get the actual number of winnings.
    $count = BingoStorage::getWinningCount($name)->count;

    // Increment the number of winnings.
    $updated = db_update('bingo')
      ->fields(array('count' => $count + 1))
      ->condition('name', $name)
      ->execute();

    return $updated;
  }

  /**
   * Get the list of most winners ordered by number of winnings.
   *
   * @param $num
   *   number of most winners to return.
   *
   * @return $winners
   *  associative array of winner names and count of winnings.
   */
  static function getWinners($num) {
    return db_select('bingo', 'b')
      ->fields('b', array('name', 'count'))
      ->orderBy('b.count', 'DESC')
      ->range(0, $num)
      ->execute()
      ->fetchAll();
  }
}

