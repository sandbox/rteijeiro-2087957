<?php

/**
 * @file
 * Contains \Drupal\bingo\Plugin\Block\BingoWinnersBlock.
 */

namespace Drupal\bingo\Plugin\Block;

use Drupal\block\BlockBase;
use Drupal\block\Annotation\Block;
use Drupal\Core\Annotation\Translation;

/**
 * Provides a 'Bingo Winners" block.
 *
 * @Block(
 *   id = "bingo_winners",
 *   admin_label = @Translation("Bingo winners")
 * )
 */
class BingoWinnersBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function settings() {
    return array(
      'cache' => DRUPAL_NO_CACHE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'max_count' => \Drupal::config('bingo.settings')->get('max_count'),
    );
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockForm().
   */
  public function blockForm($form, &$form_state) {
    $form['bingo_winners_block_max_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of winners'),
      '#default_value' => $this->configuration['max_count'],
      '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
      '#description' => t('Maximum number of winners to display.'),
    );

    return $form;
  }

  /**
   * Overrides \Drupal\block\BlockBase::blockSubmit().
   */
  public function blockSubmit($form, &$form_state) {
    $this->configuration['max_count'] = $form_state['values']['bingo_winners_block_max_count'];
    \Drupal::config('bingo.settings')->set('max_count', $form_state['values']['bingo_winners_block_max_count']);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return array(
      bingo_winners_list($this->configuration['max_count']),
    );
  }
}

